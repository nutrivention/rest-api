package com.nutriapp.dao;

import java.util.List;

import com.nutriapp.models.Order;
import com.nutriapp.models.OrderItem;

public interface OrderItemDAO {
	public void storeOrderItem(OrderItem orderItem);
	public List<OrderItem> getOrderItems(Order order);
}
