package com.nutriapp.dao;

import com.nutriapp.models.User;

public interface UserDAO {
	public boolean storeUser(User user);
	public User getUser(String email);
}
