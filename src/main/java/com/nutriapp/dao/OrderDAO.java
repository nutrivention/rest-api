package com.nutriapp.dao;

import java.util.List;

import com.nutriapp.models.Order;
import com.nutriapp.models.User;

public interface OrderDAO {
	public void storeOrder(Order order);
	public List<Order> getOrders(User user);
	public Order getOrder(String orderId);
}
