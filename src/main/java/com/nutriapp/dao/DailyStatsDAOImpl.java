package com.nutriapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.DailyStats;
import com.nutriapp.models.User;
import com.nutriapp.utils.HibernateUtil;

public class DailyStatsDAOImpl implements DailyStatsDAO{

	@Override
	public void updateStats(DailyStats ds) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(ds);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
	}

	@Override
	public List<DailyStats> userStats(User user) {
		List<DailyStats> ds =  new ArrayList<>();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			ds = session.createCriteria(DailyStats.class)
					.add(Restrictions.eq("user", user))
					.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return ds;
	}

}
