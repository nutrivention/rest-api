package com.nutriapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.Ingredient;
import com.nutriapp.utils.HibernateUtil;

public class IngredientDAOImpl implements IngredientDAO{

	@Override
	public void storeIngredient(Ingredient ingredient) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.save(ingredient);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
	}

	@Override
	public List<Ingredient> getIngredients() {
		List<Ingredient> ingredients =  new ArrayList<>();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			ingredients = session.createCriteria(Ingredient.class)
					.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return ingredients;
	}

	@Override
	public Ingredient getIngredient(String name) {
		Ingredient i = new Ingredient();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			i = (Ingredient) session.createCriteria(Ingredient.class)
					.add(Restrictions.eq("name", name))
					.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return i;
	}

}
