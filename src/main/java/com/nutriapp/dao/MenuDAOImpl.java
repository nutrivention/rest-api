package com.nutriapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.Menu;
import com.nutriapp.utils.HibernateUtil;

public class MenuDAOImpl implements MenuDAO{

	@Override
	public void storeMenu(Menu menu) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.save(menu);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
	}

	@Override
	public Menu getMenu(String name) {
		Menu m = new Menu();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			m = (Menu) session.createCriteria(Menu.class)
					.add(Restrictions.eq("name", name))
					.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return m;
	}

	@Override
	public List<Menu> getAllMenus() {
		List<Menu> m =new ArrayList<>();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			m = session.createCriteria(Menu.class)
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return m;
	}

}
