package com.nutriapp.dao;

import java.util.List;

import com.nutriapp.models.DailyStats;
import com.nutriapp.models.User;

public interface DailyStatsDAO {
	public void updateStats(DailyStats ds);
	public List<DailyStats> userStats(User user);
}
