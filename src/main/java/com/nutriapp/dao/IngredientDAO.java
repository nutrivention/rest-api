package com.nutriapp.dao;

import com.nutriapp.models.Ingredient;

import java.util.List;

public interface IngredientDAO {
	
	public void storeIngredient(Ingredient ingredient);
	public List<Ingredient> getIngredients();
	public Ingredient getIngredient(String name);
}
