package com.nutriapp.dao;

import java.util.List;

import com.nutriapp.models.Menu;

public interface MenuDAO {
	public void storeMenu(Menu menu);
	public Menu getMenu(String name);
	public List<Menu> getAllMenus();
}
