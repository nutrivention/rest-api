package com.nutriapp.dao;

import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.Menu;
import com.nutriapp.models.User;
import com.nutriapp.utils.HibernateUtil;

public class UserDAOImpl implements UserDAO{

	@Override
	public boolean storeUser(User user) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
			return false;
		}finally {
			session.close();
		}
		return true;
	}

	@Override
	public User getUser(String email) {
		User u = new User();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			u = (User) session.createCriteria(User.class)
					.add(Restrictions.eq("email", email))
					.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return u;
	}

}
