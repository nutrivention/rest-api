package com.nutriapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.Order;
import com.nutriapp.models.User;
import com.nutriapp.utils.HibernateUtil;

public class OrderDAOImpl implements OrderDAO{

	@Override
	public void storeOrder(Order order) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(order);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
	}

	@Override
	public List<Order> getOrders(User user) {
		List<Order> o = new ArrayList<>();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			o = session.createCriteria(Order.class)
					.add(Restrictions.eq("user", user))
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return o;
	}

	@Override
	public Order getOrder(String orderId) {
		Order o = null;
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			int orderIdint = Integer.parseInt(orderId);
			o = (Order) session.createCriteria(Order.class)
					.add(Restrictions.eq("id", orderIdint))
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return o;
	}

}
