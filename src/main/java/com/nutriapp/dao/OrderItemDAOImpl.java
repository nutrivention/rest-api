package com.nutriapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.nutriapp.models.Order;
import com.nutriapp.models.OrderItem;
import com.nutriapp.utils.HibernateUtil;

public class OrderItemDAOImpl implements OrderItemDAO{

	@Override
	public void storeOrderItem(OrderItem orderItem) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(orderItem);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
	}

	@Override
	public List<OrderItem> getOrderItems(Order order) {
		List<OrderItem> o = new ArrayList<>();
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			o = session.createCriteria(OrderItem.class)
					.add(Restrictions.eq("order", order))
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
	            tx.rollback();
	        }
			e.printStackTrace();
		}finally {
			session.close();
		}
		return o;
	}

}
