package com.nutriapp.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.nutriapp.dao.IngredientDAO;
import com.nutriapp.dao.IngredientDAOImpl;
import com.nutriapp.models.Ingredient;


public class StoreExcel {

	public static void main(String[] args) {
		File myFile = new File("C:/Users/Alexandru/Desktop/Nutrivention/testfiles/ingredients2.xlsx");
		try {
			FileInputStream fis = new FileInputStream(myFile);
			XSSFWorkbook wb= new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = (Row) rowIterator.next();
				Ingredient ingredient = new Ingredient();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = (Cell) cellIterator.next();
					switch (cell.getColumnIndex()) {
					case 0:
						System.out.println("Name: " + cell.getStringCellValue());
						ingredient.setName(cell.getStringCellValue());
						break;
					case 1:
						System.out.println("Carbo: "+cell.getStringCellValue());
						ingredient.setCarbo(new Double(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 2:
						System.out.println("Grasimi: "+cell.getStringCellValue());
						ingredient.setGrasimi(new Double(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 3:
						System.out.println("Proteine: "+cell.getStringCellValue());
						ingredient.setProteine(new Double(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 4:
						System.out.println("Fibre: "+cell.getStringCellValue());
						ingredient.setFibre(new Double(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 5:
						System.out.println("Zahar: "+cell.getStringCellValue());
						ingredient.setZahar(new Double(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 6:
						System.out.println("Pret: "+cell.getNumericCellValue());
						ingredient.setPretPer100g(cell.getNumericCellValue());
						break;
					case 7:
						System.out.println("PretGatit: "+cell.getNumericCellValue());
						ingredient.setPretGatit(cell.getNumericCellValue());
						break;
					case 8:
						System.out.println("Kcal: "+cell.getStringCellValue());
						ingredient.setKcal(new Integer(cell.getStringCellValue().replaceAll("[^\\d.]", "")));
						break;
					case 9:
						System.out.println("Tip: "+cell.getStringCellValue());
						ingredient.setType(cell.getStringCellValue());
						break;

					default:
						break;
					}					
				}
				IngredientDAO intdao = new IngredientDAOImpl();
				intdao.storeIngredient(ingredient);
				System.out.println("");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
