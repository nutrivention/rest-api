package com.nutriapp.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.nutriapp.dao.IngredientDAO;
import com.nutriapp.dao.IngredientDAOImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;


public class StoreMenu {

	public static void main(String[] args) {
		File myFile = new File("C:/Users/Alexandru/Desktop/Nutrivention/testfiles/menus3.xlsx");
		try {
			FileInputStream fis = new FileInputStream(myFile);
			XSSFWorkbook wb= new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			String name;
			Menu menu = null;
			MenuDAO mdao = new MenuDAOImpl();
			IngredientDAO idao = new IngredientDAOImpl();
			while (rowIterator.hasNext()) {
				
				Row row = (Row) rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = (Cell) cellIterator.next();
					if(cell.getColumnIndex() == 0){
						if(menu != null){
							mdao.storeMenu(menu);
							//System.out.println(mdao.getMenu(menu.getName()).getPrimaryIngredients().get(0).getName());
						}
						System.out.println("Menu: "+cell.getStringCellValue());
						menu = new Menu();
						menu.setName(cell.getStringCellValue());
					}
					if(cell.getColumnIndex() == 1){
						System.out.println("Tip: "+cell.getStringCellValue());
						//menu.getPrimaryIngredients().add(idao.getIngredient(cell.getStringCellValue().substring(0,1).toUpperCase()+cell.getStringCellValue().substring(1)));
						menu.setType(cell.getStringCellValue());
					}
					if(cell.getColumnIndex() == 2){
						System.out.println("Ingredient: "+cell.getStringCellValue());
						//menu.getPrimaryIngredients().add(idao.getIngredient(cell.getStringCellValue().substring(0,1).toUpperCase()+cell.getStringCellValue().substring(1)));
						String ingredientname = cell.getStringCellValue().substring(0,1).toUpperCase()+cell.getStringCellValue().substring(1);
						Ingredient i = idao.getIngredient(ingredientname);
						System.out.println(i);
						if(cellIterator.hasNext()){
							cell = (Cell) cellIterator.next();
							System.out.println("G: "+ cell.getNumericCellValue());
							menu.getIngredients().put(i, cell.getNumericCellValue());
						}else{
							menu.getIngredients().put(i, 0.);
						}
					}
				}
				System.out.println("");
			}
			if(menu != null){
				mdao.storeMenu(menu);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
