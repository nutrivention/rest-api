package com.nutriapp.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Loader {

	public static void main(String[] args) {
	DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	try {
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(new File("src/main/resources/strings-de.xml"));
		Element rootElement = document.getDocumentElement();
		NodeList strings = rootElement.getElementsByTagName("string");
		for(int i=0; i<strings.getLength();i++){
			Element element = (Element) strings.item(i);
			Node node = strings.item(i);
			System.out.println(element.getAttribute("name") + "---" +element.getChildNodes().item(0).getNodeValue());
		}
	} catch (ParserConfigurationException | SAXException | IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

	}
}
