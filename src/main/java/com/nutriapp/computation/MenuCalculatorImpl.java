package com.nutriapp.computation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DateUtil;

import com.nutriapp.dao.DailyStatsDAO;
import com.nutriapp.dao.DailyStatsDAOImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.models.DailyStats;
import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;
import com.nutriapp.models.OrderItem;
import com.nutriapp.models.User;

public class MenuCalculatorImpl implements MenuCalculator {
	
	public List<Menu> getMenu(List<Ingredient> ingredients, String type){
		List<Menu> eligibleMenus = new ArrayList<>();
		List<Menu> finalMenus = new ArrayList<>();
		MenuDAO mdao = new MenuDAOImpl();
		eligibleMenus = mdao.getAllMenus();
		for(Menu m: eligibleMenus){
			if(m.getType().equals(type)){
				List<Ingredient> verify = new ArrayList<Ingredient>();
				verify = m.getPrimaryIngredients();
				verify.removeAll(ingredients);
				for(Ingredient i : verify){
					if(i.getType().equals("principal")){
						finalMenus.add(m);
						break;
					}
				}
			}else{
				finalMenus.add(m);
			}
			
		}
		eligibleMenus.removeAll(finalMenus);
		return eligibleMenus;
	}

	/**
	 * 3 proposed Menus for a User
	 */
	@Override
	public List<OrderItem> getMenus(User user, String type) {
		List<Menu> eligibleMenus = new ArrayList<>();
		eligibleMenus = getMenu(user.getIngredients(), type);
		Collections.shuffle(eligibleMenus);
		List<OrderItem> ois = new ArrayList<>();
		if(eligibleMenus.size()>2){
			ois.add(computePersonalMenu(user, eligibleMenus.get(0)));
			ois.add(computePersonalMenu(user, eligibleMenus.get(1)));
			ois.add(computePersonalMenu(user, eligibleMenus.get(2)));
		}else{
			for(Menu m : eligibleMenus){
				ois.add(computePersonalMenu(user, m));
			}
		}
		return ois;
	}

	/**
	 * Compute price for personal Menu
	 * ?Map<ingredient, g>?
	 */
	
	@Override
	public double userREE(User user) {
		double ree = 0;
		if(user.getGender().equals("male")){
			ree = 10*user.getWeight() + 6.25*user.getHeight() - 5*user.getAge() + 5;
		}else{
			ree = 10*user.getWeight() + 6.25*user.getHeight() - 5*user.getAge() -161;
		}
		switch (user.getActivity()) {
		case "sedentar":
			ree*= 1.2;
			break;
		case "usor":
			ree*= 1.375;
			break;
		case "moderat":
			ree*= 1.55;
			break;
		case "foarte":
			ree*= 1.725;
			break;

		default:
			ree*= 1.2;
			break;
		}
		return ree;
	}
	public double remainingRee(User user){
		double ree= userREE(user);
		DailyStatsDAO dsdao = new DailyStatsDAOImpl();
		List<DailyStats> ds = new ArrayList<>();
		ds = dsdao.userStats(user);
		Date today = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("ddMMyyyy");
		for(DailyStats d :ds){
			Date date = d.getDate();
			if(fmt.format(date).equals(fmt.format(today))){
				return ree-d.getReeconsumed();
			}
		}
		return ree;
	}
	public double remainingfibre(User user){
		double fibre =20;
		DailyStatsDAO dsdao = new DailyStatsDAOImpl();
		List<DailyStats> ds = new ArrayList<>();
		ds = dsdao.userStats(user);
		Date today = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("ddMMyyyy");
		for(DailyStats d :ds){
			Date date = d.getDate();
			if(fmt.format(date).equals(fmt.format(today))){
				return fibre-d.getFibrecosumed();
			}
		}
		return fibre;
	}
	public int remainingMeals(User user){
		int meals = user.getMealsperday();
		DailyStatsDAO dsdao = new DailyStatsDAOImpl();
		List<DailyStats> ds = new ArrayList<>();
		ds = dsdao.userStats(user);
		Date today = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("ddMMyyyy");
		for(DailyStats d :ds){
			Date date = d.getDate();
			if(fmt.format(date).equals(fmt.format(today))){
				return meals - d.getMealshad();
			}
		}
		return meals;
	}

	@Override
	public OrderItem computePersonalMenu(User user, Menu menu) {
		Map<Ingredient, Double> personalMenu = new HashMap<>();
		double remainingree = remainingRee(user);
		Map<Ingredient, Double> eligibleIngredients = new HashMap<>();
		eligibleIngredients =  menu.getIngredients();
		for(Ingredient i : user.getBadIngredients()){
			//eligibleIngredients.remove(i);
		}
		double factor =1;
		double obj;
		switch (user.getObjective()) {
		case "creste":
			obj = +250;
			break;
		case "scade":
			obj = -250;
			break;
		case "mentine":
			obj = 0;
			break;
		default:
			obj=0;
			break;
		}
		//adapt factor for kcal
		factor = remainingree/8*3/menu.getDefaultKcal();
		if(menu.getDefaultKcal()>remainingree-obj){
			factor = (remainingree-obj)/menu.getDefaultKcal();
		}

		for(Map.Entry<Ingredient, Double> ingredient : eligibleIngredients.entrySet()){
			personalMenu.put(ingredient.getKey(), ingredient.getValue()*factor);
		}
		OrderItem oi = new OrderItem(personalMenu, menu.getName());
		return oi;
	}

}
