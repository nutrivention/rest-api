package com.nutriapp.computation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.nutriapp.dao.IngredientDAO;
import com.nutriapp.dao.IngredientDAOImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.dao.UserDAO;
import com.nutriapp.dao.UserDAOImpl;
import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;
import com.nutriapp.models.User;

public class ComputatonTest {
	public static void main(String[] args) {
		UserDAO udao = new UserDAOImpl();
		MenuDAO mdao = new MenuDAOImpl();
		User u = udao.getUser("anto@iol.ro");
		MenuCalculator mc = new MenuCalculatorImpl();
		System.out.println(mc.computePersonalMenu(u, mdao.getMenu("steak ribeye vita cu salata verde")));
		
	}
}
