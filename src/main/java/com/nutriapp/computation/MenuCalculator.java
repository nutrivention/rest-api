package com.nutriapp.computation;

import java.util.List;
import java.util.Map;

import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;
import com.nutriapp.models.OrderItem;
import com.nutriapp.models.User;

public interface MenuCalculator {
	public List<OrderItem> getMenus(User user, String type);
	public OrderItem computePersonalMenu(User user, Menu menu);
	public double userREE(User user);
}
