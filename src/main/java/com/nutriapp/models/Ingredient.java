package com.nutriapp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "ingredients")
@Getter@Setter
@ToString
@EqualsAndHashCode(of = "name")
public class Ingredient {
	
	@Id
	@Column(name = "name")
	String name;
	@Column(name = "carbo")
	double carbo;
	@Column(name = "grasimi")
	double grasimi;
	@Column(name = "proteine")
	double proteine;
	@Column(name = "fibre")
	double fibre;
	@Column(name = "zahar")
	double zahar;
	@Column(name = "pretlasuta")
	double pretPer100g;
	@Column(name = "pretgatit")
	double pretGatit;
	@Column(name = "kcal")
	int kcal;
	@Column(name = "type")
	String type;
}
