package com.nutriapp.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "users")
@Getter@Setter
@EqualsAndHashCode(of = "email")
@ToString
public class User {

	@Id@GeneratedValue
	@Column(name = "id")
	int id;
	@Column(name = "name")
	String name;
	@Column(name = "password")
	String password;
	@Column(name = "email")
	String email;
	@Column(name = "age")
	int age;
	@Column(name = "gender")
	String gender;
	@Column(name = "weight")
	double weight;
	@Column(name = "height")
	double height;
	@Column(name = "bodyfat")
	double bodyfat;
	@Column(name = "bodytype")
	String bodytype;
	@Column(name = "activity")
	String activity;
	@Column(name = "objective")
	String objective;
	@Column(name = "mealsperday")
	int mealsperday;
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinColumn( nullable = true, insertable=false, updatable=true)
	@JsonIgnore
	Map<Ingredient, String> ingredientsRate;
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinColumn( nullable = true, insertable=true, updatable=true)
	@Cascade(CascadeType.SAVE_UPDATE)
	Map<Menu, String> menuRate;
	
	public List<Ingredient> getBadIngredients(){
		List<Ingredient> ingredients =  new ArrayList<>();
		for(Map.Entry<Ingredient, String> e:ingredientsRate.entrySet()){
			if(e.getValue().equals("RAU") ){
				// TODO enum rates
				ingredients.add(e.getKey());
			}			
		}
		return ingredients;
	}
	
	public List<Ingredient> getIngredients(){
		List<Ingredient> ingredients =  new ArrayList<>();
		for(Map.Entry<Ingredient, String> e:ingredientsRate.entrySet()){
			if(!e.getValue().equals("RAU") ){
				// TODO enum rates
				ingredients.add(e.getKey());
			}			
		}
		return ingredients;
	}
}
