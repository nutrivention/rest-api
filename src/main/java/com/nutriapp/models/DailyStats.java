package com.nutriapp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "dailystats")
@Getter@Setter
@ToString(exclude = {"id"})
public class DailyStats {
	@Id@GeneratedValue
	@Column(name = "id")
	int id;
	@Column(name = "reeconsumed")
	double reeconsumed;
	@Column(name = "fibreconsumed")
	double fibrecosumed;
	@Column(name = "mealshad")
	int mealshad;
	@OneToOne
	User user;
	@Column(name = "date")
	Date date;
}
