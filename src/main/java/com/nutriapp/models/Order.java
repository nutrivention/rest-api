package com.nutriapp.models;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "userorder")
@Getter@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
public class Order {
	//tostring exclude ID
	@Id@GeneratedValue
	@Column(name = "id")
	int id;
	@ManyToMany(targetEntity= OrderItem.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	//@JoinColumn(name= "orderitems", referencedColumnName ="order_id")
	List<OrderItem> items;
//	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	List<Menu> menus;
	@OneToOne
	User user;
	@Column(name = "groupleaderstatus")
	String groupleaderstatus;
	@Column(name = "date")
	LocalDateTime date;
	@Column(name = "delivery")
	LocalDateTime delivery;
	@Column(name = "status")
	String status;
	@Column(name = "feedback")
	String feedback;
	@Column(name = "rate")
	String rate;
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinColumn( nullable = true, insertable=false, updatable=false)
	Map<User, String> participants;
	@Column(name = "paymentmethod")
	String paymentmethod;
	@Column(name = "destination")
	String destination;
}
