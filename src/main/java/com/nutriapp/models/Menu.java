package com.nutriapp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "menu")
@Getter@Setter
@ToString(exclude = "ingredients")
@EqualsAndHashCode(exclude = {"ingredients", "type"})
public class Menu {

	@Id
	@Column(name = "name")
	String name;
	@Column(name = "type")
	String type;
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinColumn( nullable = true, insertable=false, updatable=false)
	Map<Ingredient, Double> ingredients;

	public Menu() {
		super();
		ingredients = new HashMap<>();
	}
	
	public double getGramms(){
		double gramms =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			gramms += e.getValue();
		}
		return gramms;
	}

	public double getDefaultKcal(){
		double kcal =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			kcal += e.getKey().getKcal()/100.*e.getValue();
		}
		return kcal;
	}
	
	public double getDefaultFibre(){
		double fibre =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			fibre += e.getKey().getFibre()/100*e.getValue();
		}
		return fibre;
	}
	
	public List<Ingredient> getPrimaryIngredients() {
		List<Ingredient> ingredients = new ArrayList<>();
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			ingredients.add(e.getKey());
		}
		return ingredients;
	}
	public double getProteine(){
		double proteine =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			proteine += e.getKey().getProteine()/100.*e.getValue();
		}
		return proteine;
	}
	public double getCarbo(){
		double carbo =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			carbo += e.getKey().getCarbo()/100.*e.getValue();
		}
		return carbo;
	}
	public double getZahar(){
		double zahar =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			zahar += e.getKey().getZahar()/100.*e.getValue();
		}
		return zahar;
	}
	public double getGrasimi(){
		double grasimi =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			grasimi += e.getKey().getGrasimi()/100.*e.getValue();
		}
		return grasimi;
	}
	public double getFibre(){
		double fibre =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			fibre += e.getKey().getFibre()/100.*e.getValue();
		}
		return fibre;
	}

	public double getMenuPrice() {
		double price =0;
		for(Map.Entry<Ingredient, Double> i : ingredients.entrySet()){
			price += i.getKey().getPretPer100g()/100.*i.getValue();
		}
		return price*3.3;
	}

}
