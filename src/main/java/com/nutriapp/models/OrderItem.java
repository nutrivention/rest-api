package com.nutriapp.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "orderitem")
@EqualsAndHashCode
@RequiredArgsConstructor
@NoArgsConstructor
@Getter@Setter
public class OrderItem {
	
	@Id@GeneratedValue
	@Column(name = "id")
	int id;
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinColumn( nullable = true, insertable=false, updatable=false)
	@NonNull 
	Map<Ingredient, Double> ingredients;
	@Column(name = "name")
	@NonNull 
	String name;
	@ManyToOne
	Order order;
	@ManyToOne
	User user;
	public OrderItem(Menu m){
		super();
		this.ingredients =  new HashMap<>();
		for(Entry<Ingredient, Double> ingredient : m.getIngredients().entrySet()) {
			this.ingredients.put(ingredient.getKey(), ingredient.getValue());
		}
		
		this.name = m.getName();
	}
	
	public double getProteine(){
		double proteine =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			proteine += e.getKey().getProteine()/100.*e.getValue();
		}
		return proteine;
	}
	public double getCarbo(){
		double carbo =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			carbo += e.getKey().getCarbo()/100.*e.getValue();
		}
		return carbo;
	}
	public double getZahar(){
		double zahar =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			zahar += e.getKey().getZahar()/100.*e.getValue();
		}
		return zahar;
	}
	public double getGrasimi(){
		double grasimi =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			grasimi += e.getKey().getGrasimi()/100.*e.getValue();
		}
		return grasimi;
	}
	public double getFibre(){
		double fibre =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			fibre += e.getKey().getFibre()/100.*e.getValue();
		}
		return fibre;
	}
	public double getDefaultKcal(){
		double kcal =0;
		for(Map.Entry<Ingredient, Double> e : this.ingredients.entrySet()){
			kcal += e.getKey().getKcal()/100.*e.getValue();
		}
		return kcal;
	}

	public double getMenuPrice() {
		double price =0;
		for(Map.Entry<Ingredient, Double> i : ingredients.entrySet()){
			price += i.getKey().getPretPer100g()/100.*i.getValue();
		}
		return price*3.3;
	}
	
	@Override
	public String toString() {
		return "OrderItem [id=" + id + ", ingredients=" + ingredients + ", name=" + name + ", getProteine()="
				+ getProteine() + ", getCarbo()=" + getCarbo() + ", getZahar()=" + getZahar() + ", getGrasimi()="
				+ getGrasimi() + ", getFibre()=" + getFibre() + ", getDefaultKcal()=" + getDefaultKcal()
				+ ", getMenuPrice()=" + getMenuPrice() + "]";
	}
}
