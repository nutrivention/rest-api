package com.nutriapp.rest;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutriapp.computation.MenuCalculator;
import com.nutriapp.computation.MenuCalculatorImpl;
import com.nutriapp.dao.IngredientDAO;
import com.nutriapp.dao.IngredientDAOImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.dao.UserDAO;
import com.nutriapp.dao.UserDAOImpl;
import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;
import com.nutriapp.models.User;

@Path("/usermanagement")
public class UserManagement {
	//TODO: resolve with @Inject!!
	UserDAO userdao;
	
	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject verifylogin(@QueryParam("email") String email, @QueryParam("password") String password){
		JSONObject json = new JSONObject();
		userdao = new UserDAOImpl();
		User u = userdao.getUser(email);
		if(u != null){
			if(u.getPassword().equals(password)){
				try {
					json.put("success", true);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else{
				try {
					json.put("success", false);
					json.put("errors", "password");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}else{
			try {
				json.put("success", false);
				json.put("errors", "username");
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return json;
	}
	
	@POST
	@Path("getuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String getUser(@QueryParam("email") String email){
		userdao = new UserDAOImpl();
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(userdao.getUser(email));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		};
		return null;
	}
	
	@POST
	@Path("reg")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject register(@QueryParam("nume") String nume, @QueryParam("prenume") String prenume, 
			@QueryParam("email") String email, @QueryParam("password") String password){
		JSONObject json = new JSONObject();
		userdao = new UserDAOImpl();
		if(password.isEmpty()){
			try {
				json.put("success", false);
				json.put("errors", "password");
				return json;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if(userdao.getUser(email)!=null){
			try {
				json.put("success", true);
				json.put("errors", "email");
				return json;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		//TODO:email check?
		User u = new User();
		u.setName(nume+" "+prenume);
		u.setEmail(email);
		u.setPassword(password);
		if(userdao.storeUser(u)){
			try {
				json.put("success", true);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else{
			try {
				json.put("success", false);
				json.put("errors", "unknown");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return json;
	}
	
	@POST
	@Path("fakereg")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject fakeregister(@QueryParam("nume") String nume, @QueryParam("prenume") String prenume, 
			@QueryParam("email") String email, @QueryParam("password") String password) throws JSONException{
		JSONObject json = new JSONObject();
		userdao = new UserDAOImpl();
		if(password.isEmpty()){
			try {
				json.put("success", false);
				json.put("errors", "password");
				return json;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if(userdao.getUser(email)!=null){
			try {
				json.put("success", false);
				json.put("errors", "email");
				return json;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
				json.put("success", true);
		
		return json;
	}
	
	@POST
	@Path("updatebody")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject updatebody(@QueryParam("email") String email, @QueryParam("age") String age, @QueryParam("weight") String weight
			, @QueryParam("height") String height, @QueryParam("bodyfat") String bodyfat, @QueryParam("gender") String gender
			, @QueryParam("bodytype") String bodytype, @QueryParam("activity") String activity, @QueryParam("objective") String objective){
		JSONObject json = new JSONObject();
		userdao = new UserDAOImpl();
		User u = userdao.getUser(email);
		if(u != null){
			try {
				if(!age.matches("[0-9]+")){
					json.put("success", false);
					json.put("errors", "age");
					return json;
				}
				//TODO: all checks
				u.setAge(Integer.parseInt(age));
				u.setWeight(Double.parseDouble(weight));
				u.setHeight(Double.parseDouble(height));
				u.setBodyfat(Double.parseDouble(bodyfat));
				if(!gender.equals("male")&&!gender.equals("female")){
					json.put("success", false);
					json.put("errors", "gender");
					return json;
				}
				u.setGender(gender);
				u.setBodytype(bodytype);
				u.setActivity(activity);
				u.setObjective(objective);
				userdao.storeUser(u);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else{
			try {
				json.put("success", false);
				json.put("errors", "email");
				return json;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			json.put("success", true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	@POST
	@Path("getsuggestions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String getSuggesntions(@QueryParam("email") String email, @QueryParam("type") String type) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		userdao = new UserDAOImpl();
		MenuCalculator calculator = new MenuCalculatorImpl();
		return mapper.writeValueAsString(calculator.getMenus(userdao.getUser(email), type));
	}

	@POST
	@Path("rateingredients")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject rateIngredients(@QueryParam("ingredient") String ingredientName, @QueryParam("rate") String rate, @QueryParam("email") String email) throws JSONException{
		JSONObject json = new JSONObject();
		userdao =  new UserDAOImpl();
		IngredientDAO ingredientDAO = new IngredientDAOImpl();
		User user = userdao.getUser(email);
		if(user != null){
			if(ingredientDAO.getIngredient(ingredientName) != null){
				Map<Ingredient, String> rates = user.getIngredientsRate();
				rates.put(ingredientDAO.getIngredient(ingredientName), rate);
				user.setIngredientsRate(rates);
				userdao.storeUser(user);
				json.put("success", true);
				return json;
			}else{
				json.put("success", false);
				json.put("errors", "ingredient");
				return json;
			}
		}else{
			json.put("success", false);
			json.put("errors", "email");
			return json;
		}
	}
	
	@POST
	@Path("updateuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject updateUser(@QueryParam("user") String userjson) throws JSONException, JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JSONObject json = new JSONObject();
		User user = mapper.readValue(userjson, User.class);
		userdao =  new UserDAOImpl();
		userdao.storeUser(user);
		json.put("success", false);
		return json;
	}
	
	@PUT
	@Path("ratemenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject rateManu(@QueryParam("menuName") String menuName, @QueryParam("rate") String rate, @QueryParam("email") String email) throws JSONException{
		JSONObject json = new JSONObject();
		userdao =  new UserDAOImpl();
		MenuDAO mdao = new MenuDAOImpl();
		User user = userdao.getUser(email);
		if(user != null){
			if(mdao.getMenu(menuName) != null){
				Map<Menu, String> rates = user.getMenuRate();
				rates.put(mdao.getMenu(menuName), rate);
				user.setMenuRate(rates);
				userdao.storeUser(user);
				json.put("success", true);
				return json;
			}else{
				json.put("success", false);
				json.put("errors", "menu");
				return json;
			}
		}else{
			json.put("success", false);
			json.put("errors", "email");
			return json;
		}
	}
	
	@POST
	@Path("getingredients")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONArray getIngredients(@QueryParam("email") String email) throws JSONException{
	
		JSONArray jsonArray = new JSONArray();
		userdao = new UserDAOImpl();
		IngredientDAO idao = new IngredientDAOImpl();
		User user = userdao.getUser(email);
		if(user != null){
			for(Ingredient i: user.getBadIngredients()){
				JSONObject json = new JSONObject();
				json.put("name", i.getName());
				json.put("rate", "RAU");
				jsonArray.put(json);
			}
			for(Ingredient ingredient:user.getIngredients()){
				JSONObject json = new JSONObject();
				json.put("name", ingredient.getName());
				json.put("rate", user.getIngredientsRate().get(idao.getIngredient(ingredient.getName())));
				jsonArray.put(json);
			}
		}
		return jsonArray;
	}
	 
}
