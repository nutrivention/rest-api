package com.nutriapp.rest;

import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutriapp.dao.IngredientDAO;
import com.nutriapp.dao.IngredientDAOImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.dao.UserDAO;
import com.nutriapp.models.Ingredient;
import com.nutriapp.models.Menu;

@Path("getbasics")
public class GetBasics {
	UserDAO userdao;
	MenuDAO menudao;

	@Path("menus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONArray getMenus() throws JSONException{
		menudao = new MenuDAOImpl();
		JSONArray array = new JSONArray();
		for(Menu m: menudao.getAllMenus()){
			JSONObject menuObject = new JSONObject();
			menuObject.put("name", m.getName());
			menuObject.put("type", m.getType());
			//menuObject.put("rate", value);
			menuObject.put("price", m.getMenuPrice());
			menuObject.put("totalGrame", m.getGramms());
			menuObject.put("totalKcal", m.getDefaultKcal());
			menuObject.put("totalGrasimi", m.getGrasimi());
			menuObject.put("totalFibre", m.getDefaultFibre());
			menuObject.put("totalProteine", m.getProteine());
			menuObject.put("totalCarbo", m.getCarbo());
			//ingredients
			JSONArray ingredientsArray = new JSONArray();
			for(Entry<Ingredient, Double> e: m.getIngredients().entrySet()){
				JSONObject ingredient = new JSONObject();
				ingredient.put("name", e.getKey().getName());
				ingredient.put("cantitate", e.getValue());
				ingredientsArray.put(ingredient);
			}
			menuObject.put("ingredients", ingredientsArray);
			array.put(menuObject);
		}
		return array;
	}
	
	@Path("ingredients")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getIngredients() throws JsonProcessingException{
		IngredientDAO idao = new IngredientDAOImpl();
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(idao.getIngredients());
	}
	
}
