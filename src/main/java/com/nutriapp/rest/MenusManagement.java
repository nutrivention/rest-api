package com.nutriapp.rest;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutriapp.computation.MenuCalculator;
import com.nutriapp.computation.MenuCalculatorImpl;
import com.nutriapp.dao.MenuDAO;
import com.nutriapp.dao.MenuDAOImpl;
import com.nutriapp.dao.OrderDAO;
import com.nutriapp.dao.OrderDAOImpl;
import com.nutriapp.dao.OrderItemDAO;
import com.nutriapp.dao.OrderItemDAOImpl;
import com.nutriapp.dao.UserDAO;
import com.nutriapp.dao.UserDAOImpl;
import com.nutriapp.models.Order;
import com.nutriapp.models.OrderItem;
import com.nutriapp.models.User;

@Path("menusmanagement")
public class MenusManagement {
	
	@POST
	@Path("groupordercheckout")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject groupordercheckout(@HeaderParam("email") String email, @HeaderParam("groupOrderId") String groupOrderId
			) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		OrderItemDAO orderItemDAO = new OrderItemDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(email) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		//verify all ready
		if(!o.getGroupleaderstatus().equals("READY")) {
			json.put("success", false);
			json.put("errors", "You are not ready!");
			return json;
		}
		if(!o.getUser().getEmail().equals(email)) {
			json.put("success", false);
			json.put("errors", "Access denied!");
			return json;
		}
		if(o.getStatus() != null && o.getStatus().equals("NEW")) {
			json.put("success", false);
			json.put("errors", "Order already placed!");
			return json;
		}
		
		JSONArray participants = new JSONArray();
		Double totalPrice = 0.;
		Double totalUserPrice = 0.;
		JSONObject groupLeader = new JSONObject();
		groupLeader.put("email", o.getUser().getEmail());
		groupLeader.put("name", o.getUser().getName());
		//put menus name, price
		JSONArray gLOI = new JSONArray();
		totalUserPrice = 0.;
		for(OrderItem oi : orderItemDAO.getOrderItems(o)) {
			if(oi.getUser().getEmail().equals(o.getUser().getEmail())) {
				JSONObject poi = new JSONObject();
				poi.put("name", oi.getName());
				poi.put("price", oi.getMenuPrice());
				totalPrice += oi.getMenuPrice();
				totalUserPrice += oi.getMenuPrice();
				gLOI.put(poi);
			}
		}
		groupLeader.put("menus", gLOI);
		groupLeader.put("totalPrice", totalUserPrice);
		participants.put(groupLeader);
		for( Entry<User, String> p : o.getParticipants().entrySet()) {
			if(!p.getValue().contains("READY")) {
				json.put("success", false);
				json.put("errors", "Participants not ready!");
				return json;
			}
			JSONObject participant = new JSONObject();
			participant.put("email", p.getKey().getEmail());
			participant.put("name", p.getKey().getName());
			//put menus name, price
			JSONArray participantOI = new JSONArray();
			totalUserPrice = 0.;
			for(OrderItem oi : orderItemDAO.getOrderItems(o)) {
				if(oi.getUser().getEmail().equals(p.getKey().getEmail())) {
					JSONObject poi = new JSONObject();
					poi.put("name", oi.getName());
					poi.put("price", oi.getMenuPrice());
					totalPrice += oi.getMenuPrice();
					totalUserPrice += oi.getMenuPrice();
					participantOI.put(poi);
				}
			}
			participant.put("menus", participantOI);
			participant.put("totalPrice", totalUserPrice);
			participants.put(participant);
		}
		json.put("participants", participants);
		json.put("totalPrice", totalPrice);
		o.setDelivery(LocalDateTime.now().plusMinutes(58));
		json.put("deliveryTime", o.getDelivery());
		json.put("currency", "RON");
		o.setStatus("NEW");
		orderdao.storeOrder(o);
		json.put("success", true);
		return json;
	}
	
	@POST
	@Path("participantready")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject participantready(@HeaderParam("email") String email, @HeaderParam("groupOrderId") String groupOrderId,  @HeaderParam("menusList") String menusList
			) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		MenuDAO menudao = new MenuDAOImpl();
		OrderItemDAO orderItemDAO = new OrderItemDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(email) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		if((!o.getUser().getEmail().equals(email) && !o.getParticipants().containsKey(userdao.getUser(email)))) {
			json.put("success", false);
			json.put("errors", "Invalid participant!");
			return json;
		}
		if(o.getParticipants().containsKey(userdao.getUser(email)) && !o.getParticipants().get(userdao.getUser(email)).contains("JOINED")) {
			json.put("success", false);
			json.put("errors", "Invalid participant!");
			return json;
		}
		JSONObject menus = new JSONObject(menusList);
		if(menus.has("menus")){
			JSONArray menuss = menus.getJSONArray("menus");
			for(int i = 0; i < menuss.length(); i++){
				JSONObject ois = (JSONObject) menuss.get(i);
				for(int j = 0; j< ois.getInt("quantity"); j++){
					OrderItem oi = new OrderItem(menudao.getMenu((String)ois.get("menu_name")));
					oi.setUser(userdao.getUser(email));
					oi.setOrder(o);
					orderItemDAO.storeOrderItem(oi);
				}				
			}
		}
		MenuCalculator mc = new MenuCalculatorImpl();
		if(menus.has("orderItems")){
			JSONArray orderitems = menus.getJSONArray("orderItems");
			for(int i = 0; i < orderitems.length(); i++){
				JSONObject ois = (JSONObject) orderitems.get(i);
				if(userdao.getUser((String)ois.get("email")) != null){
					OrderItem oi = mc.computePersonalMenu(userdao.getUser((String)ois.get("email")), menudao.getMenu((String)ois.get("menu_name")));
					oi.setUser(userdao.getUser(email));
					oi.setOrder(o);
					orderItemDAO.storeOrderItem(oi);
				}
			}
		}
		if(o.getUser().getEmail().equals(email)) {
			o.setGroupleaderstatus("READY");
		}else {
			o.getParticipants().put(userdao.getUser(email), "READY -"+o.getParticipants().get(userdao.getUser(email)).split("-")[1]);
		}
		
		orderdao.storeOrder(o);
		json.put("success", true);
		return json;
	}
	
	@POST
	@Path("checkoutorder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject checkoutorder(@HeaderParam("email") String email, @HeaderParam("destination") String destination, @HeaderParam("menusList") String menusList, @HeaderParam("paymentMethod") String paymentmethod) throws JSONException{
		JSONObject json = new JSONObject();
		if(destination.isEmpty()){
			json.put("success", false);
			json.put("errors", "Empty Destination");
			return json;
		}
		if(paymentmethod != null && !(paymentmethod.equals("CASH") || paymentmethod.equals("CREDIT"))) {
			json.put("success", false);
			json.put("errors", "Invalid payment method!");
			return json;
		}
		UserDAO userdao = new UserDAOImpl();
		if(userdao.getUser(email) == null){
			json.put("success", false);
			json.put("errors", "Invalid email");
			return json;
		}
		
		JSONObject menus = new JSONObject(menusList);
		MenuDAO menudao = new MenuDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		OrderItemDAO orderItemDAO = new OrderItemDAOImpl();
		MenuCalculator mc = new MenuCalculatorImpl();
	//verify params
		Order o = new Order();
		o.setStatus("new");
		if(paymentmethod != null) {
			o.setPaymentmethod(paymentmethod);
		}
		o.setUser(userdao.getUser(email));
		o.setDate(LocalDateTime.now());
		o.setDestination(destination);
		///// 58 Minutes DEFAULT
		o.setDelivery(LocalDateTime.now().plusMinutes(58));
		orderdao.storeOrder(o);
		Double price = 0.;
		
		if(menus.has("menus")){
			JSONArray menuss = menus.getJSONArray("menus");
			for(int i = 0; i < menuss.length(); i++){
				JSONObject ois = (JSONObject) menuss.get(i);
				for(int j = 0; j< ois.getInt("quantity"); j++){
					OrderItem oi = new OrderItem(menudao.getMenu((String)ois.get("menu_name")));
					oi.setOrder(o);
					orderItemDAO.storeOrderItem(oi);
					price += oi.getMenuPrice();
				}				
			}
		}
		if(menus.has("orderItems")){
			JSONArray orderitems = menus.getJSONArray("orderItems");
			for(int i = 0; i < orderitems.length(); i++){
				JSONObject ois = (JSONObject) orderitems.get(i);
				if(userdao.getUser((String)ois.get("email")) != null){
					OrderItem oi = mc.computePersonalMenu(userdao.getUser((String)ois.get("email")), menudao.getMenu((String)ois.get("menu_name")));
					oi.setOrder(o);
					orderItemDAO.storeOrderItem(oi);
					price += oi.getMenuPrice();
				}
			}
		}
		//o.setPrice(price);
		//orderdao.storeOrder(o);
		json.put("success", true);
		json.put("delivery_time", o.getDelivery());
		json.put("price", price);
		json.put("currency", "RON");
		return json;
	}
	
	@POST
	@Path("participantcheckstatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject participantcheckstatus(@HeaderParam("email") String email, @HeaderParam("groupOrderId") String groupOrderId
			) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(email) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		json.put("success", true);
		JSONObject groupleader = new JSONObject();
		groupleader.put("email", o.getUser().getEmail());
		groupleader.put("name", o.getUser().getName());
		groupleader.put("status", o.getGroupleaderstatus());
		json.put("groupleader", groupleader);
		JSONArray participants = new JSONArray();
		for( Entry<User, String> p : o.getParticipants().entrySet()) {
			JSONObject participant = new JSONObject();
			participant.put("email", p.getKey().getEmail());
			participant.put("name", p.getKey().getName());
			participant.put("status", p.getValue());
			participants.put(participant);
		}
		json.put("participants", participants);
		return json;
	}
	
	@POST
	@Path("removeparticipantfromgrouporder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject removeparticipantfromgrouporder(@HeaderParam("participantEmail") String participantEmail, @HeaderParam("groupOrderId") String groupOrderId
			) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(participantEmail) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		if(o.getParticipants().get(userdao.getUser(participantEmail)) == null) {
			json.put("success", false);
			json.put("errors", "User not invited!");
			return json;
		}else {
			o.getParticipants().remove(userdao.getUser(participantEmail));
		}
		orderdao.storeOrder(o);
		json.put("success", true);
		return json;
	}
	
	@POST
	@Path("addparticipantingrouporder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject addparticipantingrouporder(@HeaderParam("participantEmail") String participantEmail, @HeaderParam("groupOrderId") String groupOrderId
			) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(participantEmail) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		if(o.getParticipants().get(userdao.getUser(participantEmail)) == null) {
			o.getParticipants().put(userdao.getUser(participantEmail), "INVITED");
		}else {
			json.put("success", false);
			json.put("errors", "User already invited!");
			return json;
		}
		orderdao.storeOrder(o);
		json.put("success", true);
		return json;
	}
	
	@POST
	@Path("participatetogrouporder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject participatetogrouporder(@HeaderParam("email") String email, @HeaderParam("groupOrderId") String groupOrderId,
			@HeaderParam("paymentMethod") String paymentmethod, @HeaderParam("participate") String participate) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(email) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(groupOrderId == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		Order o = orderdao.getOrder(groupOrderId);
		if(o == null) {
			json.put("success", false);
			json.put("errors", "Invalid groupOrderId!");
			return json;
		}
		if(!(paymentmethod.equals("CASH") || paymentmethod.equals("CREDIT"))) {
			json.put("success", false);
			json.put("errors", "Invalid payment method!");
			return json;
		}
		
		if(o.getParticipants().get(userdao.getUser(email)) == null) {
			json.put("success", false);
			json.put("errors", "User not invited!");
			return json;
		}
		if(participate.equals("true")) {
			if(o.getParticipants().get(userdao.getUser(email)).equals("JOINED")) {
				System.out.println("Already confirmed");
			}
			o.getParticipants().put(userdao.getUser(email), "JOINED -"+paymentmethod);
		}else {
			o.getParticipants().put(userdao.getUser(email), "DECLINED");
		}
		orderdao.storeOrder(o);
		json.put("success", true);
		return json;
	}
	
	@POST
	@Path("creategrouporder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject creategrouporder(@HeaderParam("email") String email, @HeaderParam("destination") String destination,
			@HeaderParam("paymentMethod") String paymentmethod, @HeaderParam("participants") String participants) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		JSONObject json = new JSONObject();
		if(userdao.getUser(email) == null) {
			json.put("success", false);
			json.put("errors", "Invalid email!");
			return json;
		}
		if(destination == null) {
			json.put("success", false);
			json.put("errors", "Invalid destination!");
			return json;
		}
		if(!(paymentmethod.equals("CASH") || paymentmethod.equals("CREDIT"))) {
			json.put("success", false);
			json.put("errors", "Invalid payment method!");
			return json;
		}
		Order o = new Order();
		o.setDate(LocalDateTime.now());
		o.setStatus("new");
		o.setUser(userdao.getUser(email));
		o.setGroupleaderstatus("NOT READY");
		o.setDestination(destination);
		Map<User, String> newParticipants = new HashMap<>();
		JSONObject participantJSON = new JSONObject(participants);
		JSONArray participantsArray = participantJSON.getJSONArray("participants");
		for(int i = 0; i< participantsArray.length(); i++) {
			if(userdao.getUser((String) participantsArray.getJSONObject(i).get("email")) != null) {
				newParticipants.put(userdao.getUser((String) participantsArray.getJSONObject(i).get("email")), "INVITED");
			}
		}
		o.setParticipants(newParticipants);
		orderdao.storeOrder(o);
		json.put("success", true);
		json.put("groupOrderId", o.getId());
		return json;
	}

	@POST
	@Path("createorder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject createorder(@HeaderParam("email") String email) throws JSONException{
		UserDAO userdao = new UserDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		Order o = new Order();
		o.setDate(LocalDateTime.now());
		o.setStatus("new");
		o.setUser(userdao.getUser(email));
		orderdao.storeOrder(o);
		JSONObject json = new JSONObject();
		json.put("id", o.getId());
		return json;
	}
	
	@PUT
	@Path("putorder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject putOrder(@HeaderParam("email") String email, @HeaderParam("orderId") String orderId, @HeaderParam("orderName") String orderName) throws JSONException{
		JSONObject json = new JSONObject();
		UserDAO userdao = new UserDAOImpl();
		MenuDAO menudao = new MenuDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		if(userdao.getUser(email) != null){
			if(orderdao.getOrder(orderId) != null){
				Order o = orderdao.getOrder(orderId);	
				System.out.println(o.getItems().size());
				if(o.getUser() != null && o.getUser().equals(userdao.getUser(email))){
					if(menudao.getMenu(orderName) != null){
						MenuCalculator mc = new MenuCalculatorImpl();
						OrderItem oi = mc.computePersonalMenu(userdao.getUser(email), menudao.getMenu(orderName));
						//save oi?
						o.getItems().add(oi);
						orderdao.storeOrder(o);
						json.put("success", true);
					}else{
						json.put("success", false);
						json.put("error", "wrong menu name");
						return json;
					}
				}else{
					json.put("success", false);
					json.put("error", "wrong orderID - user pair");
					return json;
				}
			}else{
				json.put("success", false);
				json.put("error", "wrong orderID");
				return json;
			}
		}else{
			json.put("success", false);
			json.put("error", "wrong email");
			return json;
		}
		return json;
	}
	
	@PUT
	@Path("putmenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JSONObject putMenu(@HeaderParam("email") String email, @HeaderParam("orderId") String orderId, @HeaderParam("menuName") String orderName, @HeaderParam("quantity") String quantity) throws JSONException{
		JSONObject json = new JSONObject();
		UserDAO userdao = new UserDAOImpl();
		MenuDAO menudao = new MenuDAOImpl();
		OrderDAO orderdao = new OrderDAOImpl();
		if(userdao.getUser(email) != null){
			if(orderdao.getOrder(orderId) != null){
				Order o = orderdao.getOrder(orderId);	
				System.out.println(o.getItems().size());
				if(o.getUser() != null && o.getUser().equals(userdao.getUser(email))){
					if(menudao.getMenu(orderName) != null){
						//o.getMenus().add(menudao.getMenu(orderName));
						json.put("success", true);
					}else{
						json.put("success", false);
						json.put("error", "wrong menu name");
						return json;
					}
				}else{
					json.put("success", false);
					json.put("error", "wrong orderID - user pair");
					return json;
				}
			}else{
				json.put("success", false);
				json.put("error", "wrong orderID");
				return json;
			}
		}else{
			json.put("success", false);
			json.put("error", "wrong email");
			return json;
		}
		return json;
	}


}
